#!/bin/bash

#
# This script can show these information:
# 
# - Date and Time
# - Current Workspace Number
# - Specifed Open apps (Text only tray)
#
# DEPENDENCIES
#
# - xdotool (for showing current workspace number)
#
# LICENSE
#
# MIT License
# 
# Copyright (c) 2020 Just Perfection
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

#
# Prints colorized string to the output
#
# @param string $1 text
# @param int $2 color number. for example 34.
#
# @stdout prints the string with color wrapper
#
function printf_color
{
    printf "\033[0;$2m$1\033[0m";
}

#
# generate the tray string and print it to the output
#
# @param array $1
#   key is string of tray name
#   value is string of command output. empty string means not open
#
# @stdout prints the generated tray string
#
function generate_tray
{    
    local -n arr=$1;
    declare -a local items;

    for name in "${!arr[@]}"; do
        cmd_output="${arr[$name]}";
        if [[ $cmd_output ]]
        then
            items+=($name);
        fi
    done

    items_str=${items[@]};
    printf "${items_str// /  }";
}

#
# generate the tray string and print it to the output
#
# @param int $1 workspace number
# @param string $2 date and time
# @param string $3 tray
#
# @stdout prints the generated tray string
#
function finalize
{
    local ws="$1";
    local datetime="$2";
    local tray="$3";

    printf "\n ";
    printf_color " $ws " 41;
    printf_color "  $tray " 37;
    printf "\n\n ";
    printf_color "$datetime" 34;
    printf "\n\n";
}

# string of date and time
date=$(date "+%b %d %a %I:%M %p");

# int of desktop number
xdotool_num=$(xdotool get_desktop);
ws_num=$((xdotool_num + 1));

# array of tray items
# see generate_tray() function paramter for array structure
declare -A tray;
tray['GMB']=$(pidof -x gmusicbrowser.pl);
tray['Discord']=$(pidof Discord);
tray['Spotify']=$(pidof spotify);
tray['Caffeine']=$(pidof -x caffeine);
tray['Private']=$(ip link | grep tun0);

tray_str=$(generate_tray tray);

# final output
finalize $ws_num "$date" "$tray_str";
